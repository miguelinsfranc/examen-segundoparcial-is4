# Tema 3 - Script PHP según las especificaciones del II Examen Parcial.

Este repositorio contiene un script PHP que realiza las siguientes tareas, según las especificaciones del Temario II Parcial:

- Define una constante para la cantidad de filas y columnas de una matriz.
- Genera una matriz cuadrada con números aleatorios en cada celda entre 0 y 9.
- Imprime la matriz en pantalla en formato tabular.
- Calcula la suma de las celdas que conforman la diagonal principal de la matriz.
- Si la suma de la diagonal principal está entre 10 y 15, el script termina e imprime un mensaje de finalización.
- Si no se cumple la condición anterior, el script continúa generando matrices hasta que se cumpla.

## Puntos de Evaluación (8 puntos en total):

- Se debe definir a través de una constante un número que representará la cantidad de
filas y de columnas de una matriz. (0,5pts)
- Se deberá generar una matriz cuadrada (es una matriz que posee el mismo número de
filas que de columnas) que en cada celda tenga números aleatorios generados entre 0
y 9. La cantidad de filas y columnas se toman de la constante definida previamente. (2
puntos)
- Se deberá imprimir en pantalla de manera tabular la matriz y se deberá calcular la
suma de las celdas que conforman la diagonal principal. (1 puntos)
- Si la suma de la diagonal principal está entre 10 y 15 el script termina e imprime un
mensaje de la suma y finalización del script. En cualquier otro caso, el script
continuará generando matrices hasta que se cumpla esa condición ( que la suma de la
diagonal principal está entre 10 y 15). (2,5 puntos)
- Estructurar la solución a través de a través del uso de funciones definidas por el
usuario (1 punto).
- Levantar la solución al gitlab personal (1 punto)