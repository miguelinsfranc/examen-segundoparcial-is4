<?php
define("N", 3); // Definir la cantidad de filas y columnas de la matriz

function generarMatriz() {
    $matriz = array();
    for ($i = 0; $i < N; $i++) {
        $fila = array();
        for ($j = 0; $j < N; $j++) {
            $fila[] = rand(0, 9); // Generar un número aleatorio entre 0 y 9
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

function imprimirMatriz($matriz) {
    foreach ($matriz as $fila) {
        echo "| ";
        foreach ($fila as $celda) {
            echo $celda . " | ";
        }
        echo PHP_EOL;
    }
}

function sumarDiagonalPrincipal($matriz) {
    $suma = 0;
    for ($i = 0; $i < N; $i++) {
        $suma += $matriz[$i][$i];
    }
    return $suma;
}

do {
    $matriz = generarMatriz();
    imprimirMatriz($matriz);
    $sumaDiagonalPrincipal = sumarDiagonalPrincipal($matriz);
} while ($sumaDiagonalPrincipal < 10 || $sumaDiagonalPrincipal > 15);

echo "La suma de la diagonal principal es: " . $sumaDiagonalPrincipal . ". El script ha finalizado.";
?>
